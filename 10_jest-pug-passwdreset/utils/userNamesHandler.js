export const userNamesHandler = (name) => {
  if (typeof name !== 'string') return '';

  const tempUserNameArr = name
    .normalize('NFD')
    .replace(/\p{Diacritic}/gu, '')
    .toLowerCase()
    .replace(/[^-a-z]/g, ' ')
    .split(' ');

  // "Jimi__ hendriX" => ["jimi", "", "", "hendrix"]

  const resultArr = [];

  for (const item of tempUserNameArr) {
    if (item) resultArr.push(item[0].toUpperCase() + item.slice(1));
  }

  return resultArr.join(' ');
};
