import { Router } from 'express';

import { homePage, todosPage } from '../controllers/viewController.js';

const router = Router();

router.get('/home', homePage);
router.get('/todos', todosPage);

export { router };
