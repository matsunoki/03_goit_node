import { getUserByEmailService, loginUser, resetPasswordService, signupUser } from '../services/userService.js';
import { catchAsync } from '../utils/catchAsync.js';

export const signup = catchAsync(async (req, res) => {
  const { newUser, token } = await signupUser(req.body);

  res.status(201).json({
    user: newUser,
    token,
  });
});

export const login = catchAsync(async (req, res) => {
  const { user, token } = await loginUser(req.body);

  res.status(200).json({
    user,
    token,
  });
});

export const forgotPassword = catchAsync(async (req, res) => {
  // validate req.body (is email?)

  const user = await getUserByEmailService(req.body.email);

  if (!user) return res.status(200).json({ msg: 'Password reset instructions sent by email' });

  const otp = user.createPasswordResetToken();
  console.log('>>>>>>>>>>>>>>>>>>>>>>>');
  console.log({ otp });
  console.log('<<<<<<<<<<<<<<<<<<<<<<<');

  await user.save();

  // send otp via email

  res.status(200).json({ msg: 'Password reset instructions sent by email' });
});

export const resetPassword = catchAsync(async (req, res) => {
  // password must be validated

  await resetPasswordService(req.params.otp, req.body.password);

  res.sendStatus(200);
});
