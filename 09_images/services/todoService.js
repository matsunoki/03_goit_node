import { userRoles } from '../constants/userRoles.js';
import { Todo } from '../models/todoModel.js';
import { HttpError } from '../utils/httpError.js';

export const createTodo = ({ title, description, dueDate }, owner) =>
  Todo.create({
    title,
    description,
    dueDate,
    owner: owner.id,
  });

export const getTodos = async (query, currentUser) => {
  // const todos = await Todo.find().populate('owner');
  // const todos = await Todo.find().populate({ path: 'owner', select: 'name role' });

  // const todos = await Todo.find({ title: 'example' }).sort('-description').skip(5).limit(3);

  // SEARCH FEATURE ================================
  const findOptions = query.search
    ? {
      $or: [
        { title: { $regex: query.search, $options: 'i' } },
        { description: { $regex: query.search, $options: 'i' } },
      ],
    }
    : {};

  if (currentUser.role === userRoles.USER) {
    if (query.search) {
      for (const option of findOptions.$or) {
        option.owner = currentUser;
      }
    }

    if (!query.search) findOptions.owner = currentUser;
  }

  // INIT DB QUERY ==========================
  const todoQuery = Todo.find(findOptions);

  // SORTING FEATURE =========================
  // order = 'ASC' | 'DESC'
  // sort = 'title' | '-title'
  todoQuery.sort(`${query.order === 'DESC' ? '-' : ''}${query.sort ?? 'title'}`);

  // PAGINATION FEATURE ======================
  // page 1 = limit 3, skip 0
  // page 2 = limit 3, skip 3
  // page 3 = limit 3, skip 6
  const page = query.page ? +query.page : 1;
  const limit = query.limit ? +query.limit : 3;
  const docsToSkip = (page - 1) * limit;

  todoQuery.skip(docsToSkip).limit(limit);

  // FETCH & COUNT DOCS ========================
  const todos = await todoQuery;
  const total = await Todo.countDocuments(findOptions);

  return { todos, total };
};

export const getTodo = async (id, currentUser) => {
  const todo = await Todo.findById(id);

  if (!todo || (currentUser.role === userRoles.USER && todo.owner.toString() !== currentUser.id)) {
    throw new HttpError(404, 'Not found..');
  }

  return todo;
};
