import { Router } from 'express';
import { checkLoginData, checkSignupData } from '../middlewares/authMiddlewares.js';
import { login, signup } from '../controllers/authController.js';

const router = Router();

router.post('/signup', checkSignupData, signup);
router.post('/login', checkLoginData, login);

export { router };
