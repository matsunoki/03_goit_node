import { Router } from 'express';

import { protect } from '../middlewares/authMiddlewares.js';
import { create, getAll, getOne } from '../controllers/todoController.js';

const router = Router();

router.use(protect);
router.post('/', create);
router.get('/', getAll);
router.get('/:id', getOne);

export { router };
