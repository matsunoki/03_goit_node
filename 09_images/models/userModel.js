import { model, Schema } from 'mongoose';
import bcrypt from 'bcrypt';
import crypto from 'crypto';

import { userRoles } from '../constants/userRoles.js';

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      select: false,
    },
    year: Number,
    role: {
      type: String,
      enum: Object.values(userRoles), // ['admin', 'user', 'moderator'],
      default: userRoles.USER,
    },
    avatar: String,
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

// userSchema.pre('find', function() {
// find hook
// });
// userSchema.pre(/^find/, function() {
// all find operations hook
// });

// Pre-save hook fires on "save" and "create" methods.
userSchema.pre('save', async function(next) {
  if (this.isNew) {
    const emailHash = crypto.createHash('md5').update(this.email).digest('hex');

    console.log('>>>>>>>>>>>>>>>>>>>>>>>');
    console.log({ emailHash });
    console.log('<<<<<<<<<<<<<<<<<<<<<<<');

    this.avatar = `https://gravatar.com/avatar/${emailHash}.jpg?d=robohash`;
  }

  if (!this.isModified('password')) return next();

  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);

  next();
});

// const isPasswordValid = await bcrypt.compare('Pass_1234', passwordHash);
userSchema.methods.checkUserPassword = (candidate, passwordHash) =>
  bcrypt.compare(candidate, passwordHash);

export const User = model('User', userSchema);
