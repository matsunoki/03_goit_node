import { Types } from 'mongoose';

import { HttpError } from '../utils/httpError.js';
import { catchAsync } from '../utils/catchAsync.js';
import { createUserDataValidator, updateUserDataValidator } from '../utils/userValidators.js';
import { checkUserExistsService, getUserByIdService } from '../services/userService.js';
import { ImageService } from '../services/imageService.js';

export const checkCreateUserData = catchAsync(async (req, res, next) => {
  const { value, errors } = createUserDataValidator(req.body);

  if (errors) throw new HttpError(400, 'Invalid user data..', errors);

  const userExists = await checkUserExistsService({ email: value.email });

  if (userExists) throw new HttpError(409, 'User with that email already exists..');

  req.body = value;

  next();
});

export const checkUpdateUserData = (req, res, next) => {
  const { value, errors } = updateUserDataValidator(req.body);

  if (errors) throw new HttpError(400, 'Invalid user data..', errors);

  req.body = value;

  next();
};

export const checkUserId = catchAsync(async (req, res, next) => {
  const { id } = req.params;

  const idIsValid = Types.ObjectId.isValid(id);

  if (!idIsValid) throw new HttpError(404, 'User not found..');

  const user = await getUserByIdService(id);

  if (!user) throw new HttpError(404, 'User not found..');

  req.user = user;

  next();
});

// BASIC MULTER USAGE
// config storage
// const multerStorage = multer.diskStorage({
//   destination: (req, file, cbk) => {
//     cbk(null, path.join('public', 'images'));
//   },
//   filename: (req, file, cbk) => {
//     const extension = file.mimetype.split('/')[1];

//     // <userId>-<randomId>.<extension>
//     cbk(null, `${req.user.id}-${v4()}.${extension}`);
//   },
// });

// // config filter
// const multerFilter = (req, file, cbk) => {
//   if (file.mimetype.startsWith('image/')) {
//     cbk(null, true);
//   } else {
//     cbk(new HttpError(400, 'Please, upload images only..'), false);
//   }
// };

// // create multer middleware
// export const uploadAvatar = multer({
//   storage: multerStorage,
//   fileFilter: multerFilter,
//   limits: {
//     fieldSize: 2 * 1024 * 1024,
//   },
// }).single('avatar');
export const uploadAvatar = ImageService.initUploadImageMiddleware('avatar');
