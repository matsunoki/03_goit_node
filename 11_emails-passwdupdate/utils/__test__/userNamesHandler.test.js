import { userNamesHandler } from '../userNamesHandler.js';

// Simple test example
// const buggyCalc = (a, b) => (a + b === 9 ? 10 : a + b);
const buggyCalc = (a, b) => a + b;

describe('Calc test example', () => {
  // test 1
  test('calc 1 + 1 = 2', () => {
    const result = buggyCalc(1, 1);

    expect(result).toBe(2);
  });

  // test 2
  it('should return 9', () => {
    expect(buggyCalc(4, 5)).toBe(9);
  });
});

// User name handler tests
const testingData = [
  { input: 'Jimi Hendrix', output: 'Jimi Hendrix' },
  { input: 'jimi hendrix', output: 'Jimi Hendrix' },
  { input: 'jimi Hendrix', output: 'Jimi Hendrix' },
  { input: '   Jimi  hendriX ', output: 'Jimi Hendrix' },
  { input: 'Jimi_Hendrix', output: 'Jimi Hendrix' },
  { input: 'jimi.hendrix', output: 'Jimi Hendrix' },
  { input: 'jimi@hend@rix', output: 'Jimi Hend Rix' },
  { input: '_jimi * hendrix', output: 'Jimi Hendrix' },
  { input: 'jimi hèndrix__', output: 'Jimi Hendrix' },
  { input: 'jimi中村hèndrix__', output: 'Jimi Hendrix' },
  { input: 'jimi de Hèndrix__', output: 'Jimi De Hendrix' },
  { input: '中村哲二', output: '' },
  { input: undefined, output: '' },
  { input: null, output: '' },
  { input: true, output: '' },
];

describe('User name handler tests', () => {
  // it ('shoul return "Jimi Hendrix"', () => {
  //   expect(userNamesHandler(testingData[0].input)).toBe(testingData[0].output);
  // })

  test('all test cases', () => {
    for (const item of testingData) {
      expect(userNamesHandler(item.input)).toBe(item.output);
    }
  });
});
