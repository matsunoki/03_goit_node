import { Todo } from '../models/todoModel.js';
import { catchAsync } from '../utils/catchAsync.js';

export const homePage = (req, res) => {
  res.status(200).render('home', {
    title: 'Todos app home page!!!',
    active: 'home',
  });
};

export const todosPage = catchAsync(async (req, res) => {
  const todos = await Todo.find().populate('owner');

  res.status(200).render('todos', {
    title: 'Todos list!!!',
    active: 'todos',
    todos,
  });
});
