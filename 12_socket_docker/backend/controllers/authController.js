import {
  getUserByEmailService,
  loginUser,
  resetPasswordService,
  signupUser,
} from '../services/userService.js';
import { catchAsync } from '../utils/catchAsync.js';
import { Email } from '../services/emailService.js';

export const signup = catchAsync(async (req, res) => {
  const { newUser, token } = await signupUser(req.body);

  try {
    const url = `${req.protocol}://${req.get('host')}/home`;
console.log('>>>>>>>>>>>>>>>>>>>>>>>');
console.log({ url });
console.log('<<<<<<<<<<<<<<<<<<<<<<<');

    await new Email(newUser, url).sendHello();
  } catch (err) {
    console.log(err);
  }

  res.status(201).json({
    user: newUser,
    token,
  });
});

export const login = catchAsync(async (req, res) => {
  const { user, token } = await loginUser(req.body);

  res.status(200).json({
    user,
    token,
  });
});

export const forgotPassword = catchAsync(async (req, res) => {
  // validate req.body (is email?)

  const user = await getUserByEmailService(req.body.email);

  if (!user) return res.status(200).json({ msg: 'Password reset instructions sent by email' });

  const otp = user.createPasswordResetToken();

  console.log('>>>>>>>>>>>>>>>>>>>>>>>');
  console.log({ otp });
  console.log('<<<<<<<<<<<<<<<<<<<<<<<');

  try {
    // // 1. setup email sendig transport
    // const emailTransport = nodemailer.createTransport({
    //   // service: 'Sendgrid'
    //   host: process.env.MAILTRAP_HOST,
    //   port: +process.env.MAILTRAP_PORT,
    //   auth: {
    //     user: process.env.MAILTRAP_USER,
    //     pass: process.env.MAILTRAP_PASS,
    //   },
    // });
    // // 2. config email
    // const emailConfig = {
    //   from: 'Todos app admin <admin@example.com>',
    //   to: 'test@example.com',
    //   subject: 'Password reset testing',
    //   html: '<h1>Test email</h1>',
    //   text: 'Test email (text version)',
    // };
    // // 3. send email
    // await emailTransport.sendMail(emailConfig);

    // const resetUrl = `https://realFrontentDomain/reset-password/${otp}`
    const resetUrl = `${req.protocol}://${req.get('host')}/api/v1/reset-password/${otp}`;

    await new Email(user, resetUrl).passwordReset();
  } catch (err) {
    user.passwordResetToken = undefined;
    user.passwordResetTokenExp = undefined;

    await user.save();
  }

  res.status(200).json({ msg: 'Password reset instructions sent by email' });
});

export const resetPassword = catchAsync(async (req, res) => {
  // password must be validated

  await resetPasswordService(req.params.otp, req.body.password);

  res.sendStatus(200);
});
