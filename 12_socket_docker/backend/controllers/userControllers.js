import {
  createUserService,
  deleteUserService,
  getUsersService,
  updateMeService,
  updateUserService,
} from '../services/userService.js';
import { catchAsync } from '../utils/catchAsync.js';

export const createUser = catchAsync(async (req, res) => {
  const newUser = await createUserService(req.body);

  res.status(201).json({
    user: newUser,
  });
});

export const getUsersList = catchAsync(async (req, res) => {
  const users = await getUsersService();
  // const users = await User.find().select('name role');
  // const users = await User.find().select('-password');
  // const users = await User.find().select('+password');

  res.status(200).json({
    users,
  });
});

export const getOneUser = (req, res) => {
  const { user } = req;

  res.status(200).json({
    user,
  });
};

export const updateUser = catchAsync(async (req, res) => {
  const { user, body } = req;

  const updatedUser = await updateUserService(user, body);

  res.status(200).json({
    user: updatedUser,
  });
});

export const deleteUser = catchAsync(async (req, res) => {
  await deleteUserService(req.user.id);

  // res.status(200).json({
  //   msg: 'success!',
  // });

  res.sendStatus(204);
});

export const getMe = (req, res) => {
  res.status(200).json({
    user: req.user,
  });
};

export const updateMe = catchAsync(async (req, res) => {
  const updatedUser = await updateMeService(req.body, req.user, req.file);

  res.status(200).json({
    user: updatedUser,
  });
});

export const updateMyPassword = (req, res) => {
  res.status(200).json({
    user: req.user,
  });
};
