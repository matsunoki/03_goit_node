import crypto from 'crypto';

import { userRoles } from '../constants/userRoles.js';
import { User } from '../models/userModel.js';
import { HttpError } from '../utils/httpError.js';
import { userNamesHandler } from '../utils/userNamesHandler.js';
import { ImageService } from './imageService.js';
import { signToken } from './jwtService.js';

/**
 * Create user service.
 * @param {Object} userData
 * @returns {Promise<User>}
 *
 * @author Sergii
 * @cathegory services
 */
export const createUserService = async (userData) => {
  const newUser = await User.create(userData);

  // const newUser = User(userData);

  // await newUser.save();

  newUser.password = undefined;

  return newUser;
};

/**
 * Get users list
 * @returns {Promise<Object>}
 */
export const getUsersService = () => User.find();

/**
 * Update user
 */
export const updateUserService = (user, userData) => {
  // const updatedUser = await User.findByIdAndUpdate(user.id, userData, { new: true });

  Object.keys(userData).forEach((key) => {
    user[key] = userData[key];
  });

  return user.save();

  // return updatedUser;
};

/**
 * Delete user
 */
export const deleteUserService = (id) => User.findByIdAndDelete(id);

/**
 * Check if user exists by filter
 */
export const checkUserExistsService = (filter) => User.exists(filter);

/**
 * Get user by id
 */
export const getUserByIdService = (id) => User.findById(id);

/**
 * Get user by email
 */
export const getUserByEmailService = (email) => User.findOne({ email });

export const signupUser = async ({ name, ...restUserData }) => {
  const newUser = await User.create({
    ...restUserData,
    name: userNamesHandler(name),
    role: userRoles.USER,
  });

  newUser.password = undefined;
  const token = signToken(newUser.id);

  return { newUser, token };
};

export const loginUser = async ({ email, password }) => {
  const user = await User.findOne({ email }).select('+password');

  if (!user) throw new HttpError(401, 'Unauthorized..');

  const passwordIsValid = await user.checkUserPassword(password, user.password);

  if (!passwordIsValid) throw new HttpError(401, 'Unauthorized..');

  user.password = undefined;
  const token = signToken(user.id);

  return { user, token };
};

export const updateMeService = async (userData, user, file) => {
  if (file) {
    // user.avatar = file.path.replace('public', '');
    user.avatar = await ImageService.saveImage(
      file,
      {
        maxFileSize: 2,
        width: 200,
        height: 200,
      },
      'images',
      'users',
      user.id
    );
  }

  Object.keys(userData).forEach((key) => {
    user[key] = userData[key];
  });

  return user.save();
};

export const resetPasswordService = async (otp, newPassword) => {
  const otpHash = crypto.createHash('sha256').update(otp).digest('hex');

  const user = await User.findOne({
    passwordResetToken: otpHash,
    passwordResetTokenExp: { $gt: Date.now() },
  });

  if (!user) throw new HttpError(400, 'Token is invalid..');

  user.password = newPassword;
  user.passwordResetToken = undefined;
  user.passwordResetTokenExp = undefined;

  await user.save();
};

export const checkAndUpdatePassword = async (userId, currentPasswd, newPasswd) => {
  const currentUser = await User.findById(userId).select('+password');

  const passwordIsValid = await currentUser.checkUserPassword(currentPasswd, currentUser.password);

  if (!passwordIsValid) throw new HttpError(401, 'Current password invalid!');

  currentUser.password = newPasswd;

  await currentUser.save();
};
