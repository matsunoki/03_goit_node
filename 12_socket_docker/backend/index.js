import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import morgan from 'morgan';
import mongoose from 'mongoose';
import { Server } from 'socket.io';

import { router as userRouter } from './routes/userRouter.js';
import { router as authRouter } from './routes/authRouter.js';
import { router as todoRouter } from './routes/todoRouter.js';
import { router as viewRouter } from './routes/viewRouter.js';
import { globalErrorHandler } from './controllers/errorController.js';

dotenv.config();

const app = express();

mongoose
  .connect(process.env.MONGODB_URL)
  .then(() => {
    console.log('Mongo DB connected..');
  })
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });

// MIDDLEWARE =====================
if (process.env.NODE_ENV === 'development') app.use(morgan('dev'));

// built-in
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

// setup PUG template engine
app.set('view engine', 'pug');
app.set('views', 'views');

// custom global middleware
app.use((req, res, next) => {
  console.log('Hello from middleware!!');

  // req.time = new Date().toLocaleString();

  next();
});

// CONTROLLERS ======================
// check server health
app.get('/ping', (req, res) => {
  // res.send('<h1>Hello from express!!</h1>');
  // res.sendStatus(200);
  res.status(200).json({
    status: 'success',
    msg: 'pong!',
    test: null,
  });
});

// ROUTES ==========================
const pathPrefix = '/api/v1';

app.use(`${pathPrefix}/auth`, authRouter);
app.use(`${pathPrefix}/users`, userRouter);
app.use(`${pathPrefix}/todos`, todoRouter);
app.use('/', viewRouter);

// handle not found error
app.all('*', (req, res) => {
  res.status(404).json({
    msg: 'Oops! Resource not found!',
  });
});

app.use(globalErrorHandler);

// SERVER INIT =================
// const port = process.env.PORT ? +process.env.PORT : 3001;
const port = +process.env.PORT;

const server = app.listen(port, () => {
  console.log(`Server is up and running on port ${port}`);
});

// SOCKET.IO ============================
const io = new Server(server);

// Ex.1 - basic ===============
// io.on('connection', (socket) => {
//   console.log('Client connected!');

//   // create event
//   socket.emit('message', { msg: 'Hello from socket' });

//   // listen event
//   socket.on('custom', (data) => {
//     console.log({ data });
//   });
// });

// Ex.2 - simple chat ==========
// io.on('connection', (socket) => {
//   socket.on('message', (msg) => {
//     io.emit('message', msg);
//   });
// });

// Ex.3 - final chat with rooms =======

const nodeNameSpace = io.of('/nodeNameSpace');

nodeNameSpace.on('connection', (socket) => {
  socket.on('join', (data) => {
    socket.join(data.room);

    const msg = `${data.nick ? '' : 'New user '}joined ${data.room} room`;

    nodeNameSpace.in(data.room).emit('message', { msg, nick: data.nick });
  });

  socket.on('message', (data) => {
    nodeNameSpace.in(data.room).emit('message', { msg: data.msg, nick: data.nick });
  });
});
