import { Router } from 'express';
import { checkLoginData, checkSignupData } from '../middlewares/authMiddlewares.js';
import { forgotPassword, login, resetPassword, signup } from '../controllers/authController.js';

const router = Router();

router.post('/signup', checkSignupData, signup);
router.post('/login', checkLoginData, login);

// PASSWORD RESTORING
// 1. make request to send password reset instruction via email => OTP (long token)
router.post('/forgot-password', forgotPassword);
// 2. update user password (using OTP)
router.post('/restore-password/:otp', resetPassword);

export { router };
