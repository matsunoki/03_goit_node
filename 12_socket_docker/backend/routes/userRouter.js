import { Router } from 'express';

import {
  createUser,
  deleteUser,
  getMe,
  getOneUser,
  getUsersList,
  updateMe,
  updateMyPassword,
  updateUser,
} from '../controllers/userControllers.js';
import {
  checkAndUpdateMyPassword,
  checkCreateUserData,
  checkUpdateUserData,
  checkUserId,
  uploadAvatar,
} from '../middlewares/userMiddlewares.js';
import { allowFor, protect } from '../middlewares/authMiddlewares.js';
import { userRoles } from '../constants/userRoles.js';

const router = Router();

/**
 * REST api (Create, Read, Update, Delete)
 * POST, GET, PATCH (PUT), DELETE
 *
 * POST         /users
 * GET          /users
 * GET          /users/<userId>
 * PATCH (PUT)  /users/<userId>
 * DELETE       /users/<userId>
 */
// router.post('/', createUser);
// router.get('/', getUsersList);
// router.get('/:id', checkUserId, getOneUser);
// router.patch('/:id', checkUserId, getOneUser);
// router.delete('/:id', checkUserId, getOneUser);

router.use(protect);
router.get('/me', getMe);
router.patch('/me', uploadAvatar, updateMe);
router.patch('/password', checkAndUpdateMyPassword, updateMyPassword);

router.use(allowFor(userRoles.ADMIN, userRoles.MODERATOR));
router
  .route('/')
  .post(checkCreateUserData, createUser)
  .get(getUsersList);

router.use('/:id', checkUserId);
router
  .route('/:id')
  .get(getOneUser)
  .patch(checkUpdateUserData, updateUser)
  .delete(deleteUser);

export { router };
