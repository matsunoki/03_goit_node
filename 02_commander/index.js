// environment variables
// process.env.TEST = 'Hello!!';
// console.log(process.env);

// current working dir
// console.log(process.cwd());

// array of arguments
// console.log(process.argv);

// stop script execution
// process.exit();

// console.log(global);

// global paths
// console.log(__dirname);
// console.log(__filename);

import 'colors';
import readline from 'readline';
import { promises as fs } from 'fs';
import { program } from 'commander';

program.option('-f, --file <type>', 'file for saving game results', 'game_results.log');
program.parse(process.argv);

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// example of readline usage
// rl.on('line', (txt) => {
//   console.log(txt);
//   process.exit();
// });

/** Counter of attempts */
let counter = 0;

/** Gussed number - from 1 to 10 */
const mind = Math.ceil(Math.random() * 10);

/** Path to the logs */
const logFile = program.opts().file;

/**
 * Simple input number validation
 * @category validator
 * @author Sergii
 *
 * @param {number} num - value to validate
 * @returns {boolean}
 */
const isValid = (num) => {
  if (!Number.isNaN(num) && num > 0 && num <= 10) return true;

  if (Number.isNaN(num)) console.log('Please, enter a number!'.red);
  if (num < 1 || num > 10) console.log('Number should be between 1 and 10'.red);

  return false;
};

/**
 * Write game results into the log file.
 * @category logger
 * @author Sergii
 *
 * @param {string} msg - message to write
 * @param {string} logFile - path to the log file
 * @returns {Promise<void>} - it means returning promise with undefined | null
 */
const logger = async (msg, logFile) => {
  try {
    console.log(msg.magenta);

    await fs.appendFile(logFile, `\n${new Date().toLocaleString('uk-UA')} - ${msg}`);

    console.log(`Successfully saved game results to the log file ${logFile}`);
  } catch (err) {
    console.log(`Something went very wrong.. ${err.message}`.red);
  }
};

/**
 * Main game cycle
 */
const game = () => {
  rl.question('Please, enter any whole number from 1 to 10!\n'.green, (value) => {
    // convert value to number
    // const number = Number(value);
    const number = +value;

    // validate number
    if (!isValid(number)) return game();

    // add attempt counter
    // counter = counter + 1;
    // counter++;
    // ++counter;
    counter += 1;

    // if number is not equal mind
    if (number !== mind) {
      console.log('Oh no!! Try again..'.red);

      return game();
    }

    // if number is equal mind
    logger(`Congratulations!! You guessed the number in ${counter} attempts(s) :]`, logFile);

    // exit the game
    // process.exit();
    rl.close();
  });
};
game();
