import { User } from '../models/userModel.js';

/**
 * Create user service.
 * @param {Object} userData
 * @returns {Promise<User>}
 *
 * @author Sergii
 * @cathegory services
 */
export const createUserService = async (userData) => {
  const newUser = await User.create(userData);

  // const newUser = User(userData);

  // await newUser.save();

  newUser.password = undefined;

  return newUser;
};

/**
 * Get users list
 * @returns {Promise<Object>}
 */
export const getUsersService = () => User.find();

/**
 * Update user
 */
export const updateUserService = (user, userData) => {
  // const updatedUser = await User.findByIdAndUpdate(user.id, userData, { new: true });

  Object.keys(userData).forEach((key) => {
    user[key] = userData[key];
  });

  return user.save();

  // return updatedUser;
};

/**
 * Delete user
 */
export const deleteUserService = (id) => User.findByIdAndDelete(id);

/**
 * Check if user exists by filter
 */
export const checkUserExistsService = (filter) => User.exists(filter);

/**
 * Get user by id
 */
export const getUserByIdService = (id) => User.findById(id);
