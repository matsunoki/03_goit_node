import { createTodo, getTodo, getTodos } from '../services/todoService.js';
import { catchAsync } from '../utils/catchAsync.js';

export const create = catchAsync(async (req, res) => {
  const newTodo = await createTodo(req.body, req.user);

  res.status(201).json({
    todo: newTodo,
  });
});

export const getAll = catchAsync(async (req, res) => {
  const { todos, total } = await getTodos(req.query, req.user);

  res.status(200).json({
    total,
    todos,
  });
});

export const getOne = catchAsync(async (req, res) => {
  const todo = await getTodo(req.params.id, req.user);

  res.status(201).json({
    todo,
  });
});
