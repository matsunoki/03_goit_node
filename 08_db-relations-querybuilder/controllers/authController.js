import { loginUser, signupUser } from '../services/userService.js';
import { catchAsync } from '../utils/catchAsync.js';

export const signup = catchAsync(async (req, res) => {
  const { newUser, token } = await signupUser(req.body);

  res.status(201).json({
    user: newUser,
    token,
  });
});

export const login = catchAsync(async (req, res) => {
  const { user, token } = await loginUser(req.body);

  res.status(200).json({
    user,
    token,
  });
});
