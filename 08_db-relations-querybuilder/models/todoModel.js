import { model, Schema, Types } from 'mongoose';

const todoSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
      maxLength: 30,
      trim: true,
    },
    description: {
      type: String,
      maxLength: 500,
    },
    dueDate: {
      type: Date,
      required: true,
    },
    completed: {
      type: Boolean,
      default: false,
    },
    owner: {
      type: Types.ObjectId,
      ref: 'User',
      required: true,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export const Todo = model('Todo', todoSchema);
