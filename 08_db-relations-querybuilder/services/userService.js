import { userRoles } from '../constants/userRoles.js';
import { User } from '../models/userModel.js';
import { HttpError } from '../utils/httpError.js';
import { signToken } from './jwtService.js';

/**
 * Create user service.
 * @param {Object} userData
 * @returns {Promise<User>}
 *
 * @author Sergii
 * @cathegory services
 */
export const createUserService = async (userData) => {
  const newUser = await User.create(userData);

  // const newUser = User(userData);

  // await newUser.save();

  newUser.password = undefined;

  return newUser;
};

/**
 * Get users list
 * @returns {Promise<Object>}
 */
export const getUsersService = () => User.find();

/**
 * Update user
 */
export const updateUserService = (user, userData) => {
  // const updatedUser = await User.findByIdAndUpdate(user.id, userData, { new: true });

  Object.keys(userData).forEach((key) => {
    user[key] = userData[key];
  });

  return user.save();

  // return updatedUser;
};

/**
 * Delete user
 */
export const deleteUserService = (id) => User.findByIdAndDelete(id);

/**
 * Check if user exists by filter
 */
export const checkUserExistsService = (filter) => User.exists(filter);

/**
 * Get user by id
 */
export const getUserByIdService = (id) => User.findById(id);

export const signupUser = async (userData) => {
  const newUser = await User.create({
    ...userData,
    role: userRoles.USER,
  });

  newUser.password = undefined;
  const token = signToken(newUser.id);

  return { newUser, token };
};

export const loginUser = async ({ email, password }) => {
  const user = await User.findOne({ email }).select('+password');

  if (!user) throw new HttpError(401, 'Unauthorized..');

  const passwordIsValid = await user.checkUserPassword(password, user.password);

  if (!passwordIsValid) throw new HttpError(401, 'Unauthorized..');

  user.password = undefined;
  const token = signToken(user.id);

  return { user, token };
};
