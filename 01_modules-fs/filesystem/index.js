import { promises as fs } from 'fs';
import path from 'path';

// IIFE
(async () => {
  try {
    // READ text =============================
    const pathToTextFile = path.join('samples', 'texts', 'example.txt');
    // const pathToFile = path.resolve('samples', 'texts', 'example.txt')

    const readResult = await fs.readFile(pathToTextFile);

    await fs.appendFile(pathToTextFile, '\nNEW LINE 222 ============');

    // READ directory ======================
    const filesDir = 'samples';

    // const directoryContent = await fs.readdir(filesDir);
    // const info = await fs.lstat('samples/example.json');

    // info.isDirectory();

    // READ json ============================
    const pathToJsonFile = path.join('samples', 'example.json');

    const readJsonResult = await fs.readFile(pathToJsonFile);

    const userObject = JSON.parse(readJsonResult);

    const newUserObject = {
      ...userObject,
      role: 'admin',
    };

    await fs.writeFile(pathToJsonFile, JSON.stringify(newUserObject));
  } catch (err) {
    console.log(err);
  }
})();
