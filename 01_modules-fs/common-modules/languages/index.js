const english = require('./english');
const { french, japanese } = require('./multi');

module.exports = {
  english,
  french,
  japanese,
};
